# Version History

This project uses [Semantic Versioning v2.0.0](https://semver.org/spec/v2.0.0.html).

This Version History (Change Log) file uses the formatting convention:
# Level 1 Heading: Major Release
## Level 2 Heading: Minor Release
### Level 3 Heading: Patch Release

### Unreleased

### 0.19.9
* Log a warning message when creating a new variable in an `FpgaPeripheral` object.
  This is to give users a hint they may have misspelled an FPGA register name.
  No warning is logged if the variable name begins with underscore,
  that's probably a variable deliberately created to use within a peripheral object.
### 0.19.8
* Cover more use cases for FW dowload from Nexus archives (see 0.19.7)
### 0.19.7
* remove the slash ('/') character(s) that new version of Nexus API can return in front of a FW tarball name (when downloading)
### 0.19.6
* Supress spurious "No candidate matches version" messages
* when downloading FW package from a remote repository (Gitlab) and only a base version number is specified (e.g. `0.0.7`) prioritise the exact version match (i.e. the released package) over the latest (deveolopment) build that also matches such version number. (Note that - in theory - the development builds performed after a release should be using an incremented version number).
* Introduce HttpFileFetcher class to retrieve FW packages from a (local) web server; to test it from command line mode (Poetry shell) run something like

    $ get_alveo_image -s http://202.9.15.140:8080/bb/pst_u55c_gen3x16_xdma_3_1.0.1-main.d77c5121-vitis.2022.2.tar.xz


### 0.19.5
* Fix firmware downloader broken by NEXUS upgrade
### 0.19.4
* Add more XrtInfo parameters: `hbm_temperature`, `pcie_12v_volts`, `pcie_12v_current` etc
* Un-pin ipython dependency, to allow downstream use of newer versions
  * Note: wanted to un-pin NumPy as well, but
    [XRT is not compatible with v2](https://github.com/Xilinx/XRT/issues/8258)
* Fix bug where attributes discovered in derived `FpgaPeripheral` classes would leak into
 base class - [PERENTIE-2583](https://jira.skatelescope.org/browse/PERENTIE-2583)
### 0.19.3
* Documentation & Dependency updates
### 0.19.2
* Implement firmware loading for `ArgsAmi` driver
* Add `ArgsAmi` driver creation to `create_driver_map_info`
* command-line interface flag `--kernel` renamed to `--firmware`, reflecting updated
functionality (short flag `-f` remains)
* When downloading firmware packages from GitLab repository, sort them chronologically
 (list the latest package first) to sidestep the problem with fetching package from repository
 holding more than 100 packages (which is the maximum number of packages GitLab will report in a
 single query).
### 0.19.1
* Fix nexus download (typo in search URL)
## 0.19.0
* Add prototype `ArgsAmi` driver
  * Wrapper around `ami_tool` utility
  * Limited functionality
* Use FPGA card type to select firmware to download (defaults to u55c for backward
compatibility)
* Increment default XRT/XDMA platform to v3
* `get_alveo_image`: Add `-x` short flag for XMDA platform version, allow any number
### 0.18.1
* Fix `FpgaPeripheral._field_config` so `IclFpgaField` objects work there
## 0.18.0
* Update firmware downloader for new PST image names starting "pst-u55"
* Add `FPGA_XRT_TIMEOUT` and `FPGA_POST_FW_LOAD_DELAY` environment variables
* Add FPGA development build metadata to `FpgaPersonality.fw_version`
* Add `stop_log_to_file` function to cease register logging
* Avoid numpy deprecation warning (converting array to scalar) in `ArgsXrt`
### 0.17.6
* Set file_fetcher attributes correctly when using cached files
### 0.17.5
* Cache extracted files as well as caching FPGA image archive (extract is cpu-intensive)
### 0.17.4
* Add --cache-dir option to download.py, allowing caching and use of cached FPGA images
* Fix `convert_log`. Filter was too strict. Lines reporting a source like
  `FPGA.ArgsXrt` were incorrectly being skipped.
* Add "testbench" output mode to `convert_log`
### 0.17.3
* Fix Python 3.8 compatibility, again
* Add Python 3.8 compatibility test to CI pipeline
* Add parse dependency to pyproject.toml (wasn't being picked up downstream)
* Fix write to "[:end]" style slice of IclFpgaField
* More module-level import mappings
* Add fpga_power, fpga_temperature properties to XrtInfo
### 0.17.2
* Command-line interface: add `-v` and `-q` flags, log to stdout (unless `-i`)
* Hierarchical loggers
* ArgsXrt register read/write logging
* Add `convert_log` utility, to decode register addresses from logs
### 0.17.1
* Fix Python 3.8 compatibility
## 0.17.0
* Add `create_driver_map_info` factory function
* Move hardware monitoring out of ArgsFpgaDriver - FpgaPersonality now takes
  an FpgaHardwareInfo object directly
* Allow multi-word registers to be read/written by slices - e.g. `my_reg[2:4]`
* `ArgsSimulator`: fix bug in HBM simulation when there are non-shared memories
* `ArgsXrt.write`: accept zero-dimensional np arrays
## 0.16.0
* Debug console: support multiple FPGA personalities
* `IclFpgaField`: accept `IclField` values (bug introduced when adding
  `np.integer` support in v0.15.1)
### 0.15.1
* FpgaPersonality to drop ArgsXrt driver when going out of scope in order
  to release an Alveo card lock
* `IclFpgaField`: accept `np.integer` values
* `fpga` command line utility: change operation of `--exec` option when using
  `--console` to fix functions inside executed file not being able to see each
  other
## 0.15.0
* Remove ArgsAdapter (no downstream users)
* Remove ArgsCl (no downstream users, not compatible with BDF addressing,
  doesn't support `info` interface)
* Include a 16-bit version of -1 (i.e. 0xFFFF) as a valid value returned by
  [XRT](https://xilinx.github.io/XRT/2022.1/html/pyxrt.html#pyxrt.kernel.group_id)
  `pyxrt.kernel.group_id()`
* Replace multiple `interface` support in `FpgaPersonality` and
  `FpgaPeripheral` with a singular `driver` (simpler code!)
* Select FPGA by PCIe BDF (address) instead of index number, consistent with
  XRT command-line tools
* Console dependencies no longer installed by default
* customise `get_alveo_image` code so it can handle both the old style file
  name conventions (e.g. `cnic_u55_0.1.3-dev.2480b1eb+map.22112108.tar.xz`) and
  the new style (`cnic_u55c_gen3x16_xdma_3_0.1.4-dev.9217d177+vitis.2022.2.tar.xz`)
  in GitLab package registry
### 0.14.10
* Simulate FPGA HBM buffers in ArgsSimulator
* Update ArgsXrt for compatibility with XRT v2.15
### 0.14.9
* FileFetcher expanded to allow downloading development firmware from GitLab
### 0.14.8
* `FileFetcher` class now contains FPGA image file and map file names as
  attributes
* Add "console" extras group to pyproject.toml - some future version will not
  install interactive console dependencies by default.
* Allow `str_from_int_bytes` to be called with zero (or negative) number of
  bytes
### 0.14.7
* `get_alveo_image` command line utility to download (CNIC) FPGA firmware from
  Nexus host
### 0.14.6
* FpgaPeripheral: take a reference to parent FpgaPersonality object and use its
  logger
* str\_from\_int\_bytes: Round off the numbers returned
* Restore `fpga` & `driver` variables for use with `--exec` on command line
* Move attribute & method discovery to `FpgaUserInterface` (so it works for
  both `FpgaPeripheral` and `FpgaPersonality`)
* Use `DEBUG_FPGA` environment variable to force exposing everything to the
  control system
### 0.14.5
* Tabular display of `IclField` & `IclFpgaField` objects in debug console
### 0.14.4
* Minor change to command-line utility for better downstream compatibility
### 0.14.3
* Refactor command-line utility to allow use of derived FpgaPersonality
  classes in downstream projects
### 0.14.2
* Refactor \_wait\_for\_completion and extend timeouts
### 0.14.1
* ArgsXrt: Fix intermittent kernel call failure, add wrapper around pyxrt wait
## 0.14.0
* Remove IclFpgaBitfield. Replaced with a generic IclFpgaField object that
  supports arbitrary-sized slices of registers
## 0.13.0
* Add IclFpgaBitfield object, for representing individual bits within an ARGS
  register
## 0.12.0
* ArgsMap & ArgsSimulator constructors now take dict objects rather than file
  paths. Class methods `.create_from_file` are provided to implement the old
  behaviour.
### 0.11.1
* FpgaPeripheral:
    * Fix \_user\_attributes leakage bug. It was inadvertantly
      modifying the instance inherited from FpgaUserInterface.
    * Remove discovery configuration values from user\_attributes before
      returning it for external use.
## 0.11
* ArgsCl: restore operation of writing to registers
* FpgaPeripheral: add flags to control user\_attribute discovery:
  DISCOVER\_REGISTERS, DISCOVER\_PROPERTIES, DISCOVER\_ALL
* XrtInfo: new class to report card health & other status information
### 0.10.2
* ArgsFpgaInterface: Add arguments for partial memory buffer reads
* ArgsXrt, ArgsCl:
    * Memory buffer read/write indices now match command-line configuration order
    * Read partial memory buffers (CL still transfers the full buffer but returns
* **Warning** ArgsCl is unable to write to registers in this version!
  only the region of interest, XRT does a true partial read)
    * Speed improvements to ARGS read/write
* FpgaPersonality: Add shortcut to read_memory & write_memory functions of the
  default interface
### 0.10.1
* fpga\_cmdline: Add IPython console, multiple FPGA card support, and optional
  pyopencl installation
* ArgsXrt: Change handling of Compute Unit name (now takes only the part before
  the colon)
## 0.10.0
* FpgaPeripheral: Change attribute writable discovery mechanism & error state
  reporting.
    * Attribute discovery - the IclField object returned by each attribute now
      contains a `user_write` field, to be used by the control system. This setting is
      not checked/enforced at the ICL.
    * Error condition reporting - the returned IclField object now contains a
      `user_error` configuration field. Previously an "errors" attribute was used (a
      set of strings describing active errors).
### 0.9.1
* register\_load: Interpret offsets in text file as bytes (not words)
* IclField: Remove debug print
* str\_from\_int\_bytes: Report decimal values (were accidentally rounded)
## 0.9.0
* fpga\_cmdline: Add register text file loader, make interactive mode optional
* IclFpgaField: Fix operators (they were inadvertantly replaced by dataclass)
### 0.8.2
* ArgsSimulator: Fix bug with read/write multiple words
### 0.8.1
* FpgaPeripheral: Fix bug where \_field\_config defaults (None) were incorrectly
  replacing sensible default values from ArgsMap.
## 0.8.0
* ArgsFpgaInterface: Add memory buffer read & write functions
  (read\_memory, write\_memory)
* ArgsCl, ArgsXrt: Implement write\_memory
### 0.7.2
* Configure package to support Python v3.6+ (was 3.8+)
### 0.7.1
* Change package name to ska\_low\_cbf\_fpga (was ska-low-cbf-fpga)
## 0.7.0
* New repository
    * Splits generic FPGA interface away from software that is specific to a
      control system (Tango device) or firmware image (PSR packetiser peripheral)
## 0.6.0
* ArgsXrt created, a new ArgsFpgaDriver that works via the Xilinx XRT Python
  bindings, [pyxrt](https://xilinx.github.io/XRT/master/html/pyxrt.html)
### 0.5.4
* ArgsCl: Add support for read & write of arrays larger than exchange buffer
### 0.5.3
* IclField: Add operators and integer typecast
### 0.5.2
* Fix tests that broke in 0.5.1:
    * fpga\_icl.py: Rename imports so type checks work when testing
    * tests/conftest.py: Update patching of args\_map
      ("\_load\_map" function renamed to "load\_map")
### 0.5.1
* ArgsSimulator added, an FPGA simulation driver
* ArgsPolledAdapter: Make polling loop do reads in blocks
* IclFpgaField: Fix addressing of elements within value arrays
## 0.5.0
* Major refactor. AlveoCL split into smaller, more modular, pieces: ArgsMap, ArgsCl,
  ArgsFpgaDriver etc.
