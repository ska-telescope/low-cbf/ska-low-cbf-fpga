FPGA Image Download
-------------------
The FPGA image downloader is able to download FPGA images stored in Gitlab or SKA CAR. It can be used from the command line
or as a callable function within other code. When used from the command line usage is:

::

    usage: download.py [-h] [-s SOURCE] [-p PERSONALITY] [-v VERSION] [-n] [--platform-version {2,3}] [-c CACHE_DIR]

    Fetch FPGA image from a remote source (e.g. Nexus).

    options:
    -h, --help            show this help message and exit
    -s SOURCE, --source SOURCE
                            use the specified archive source or Nexus if omitted; valid values: nexus, gitlab (default: nexus)
    -p PERSONALITY, --personality PERSONALITY
                            image type (cnic, pst, ...) (default: cnic)
    -v VERSION, --version VERSION
                            version number; use the latest version when omitted (default: None)
    -n, --no-delete       do not delete temporary files (default: False)
    --platform-version {2,3}
                            use the specified platform version (default: 2)
    -c CACHE_DIR, --cache-dir CACHE_DIR
                            directory used as image cache (default: )

A code snippet showing an example of usage when called as a function:

.. code-block:: python

    from ska_low_cbf_fpga.download import DownloadError, get_file_fetcher

    ffetcher = get_file_fetcher("gitlab")
    personality = "cnic"
    version = ""
    platform = 3
    try:
        self._file_fetcher = ffetcher(
            personality, version, platform_version=platform
        )
    except DownloadError as ex:
        self.logger.error(f"FPGA Download error: {ex}")
        return False

    fpgamap_path = getattr(self._file_fetcher, "map_file_path", None)
    xcl_file = getattr(self._file_fetcher, "image_file_path", None)
