# -*- coding: utf-8 -*-

"""Module init code."""
__all__ = (
    "ARGS_MAGIC",
    "WORD_SIZE",
    "ArgsFpgaDriver",
    "ArgsWordType",
    "mem_parse",
    "str_from_int_bytes",
    "ArgsMap",
    "ArgsFieldInfo",
    "ArgsSimulator",
    "create_driver_map_info",
    "DISCOVER_ALL",
    "DISCOVER_PROPERTIES",
    "DISCOVER_REGISTERS",
    "IclField",
    "IclFpgaField",
    "FpgaHardwareInfo",
    "FpgaPeripheral",
    "FpgaPersonality",
    "FpgaUserInterface",
    "log_to_file",
    "stop_log_to_file",
    "ArgsAmi",
)

from .args_fpga import (
    ARGS_MAGIC,
    WORD_SIZE,
    ArgsFpgaDriver,
    ArgsWordType,
    mem_parse,
    str_from_int_bytes,
)
from .args_map import ArgsFieldInfo, ArgsMap
from .args_simulator import ArgsSimulator
from .driver import create_driver_map_info
from .fpga_icl import (
    DISCOVER_ALL,
    DISCOVER_PROPERTIES,
    DISCOVER_REGISTERS,
    FpgaPeripheral,
    FpgaPersonality,
    FpgaUserInterface,
    IclField,
    IclFpgaField,
)
from .hardware_info import FpgaHardwareInfo
from .log import log_to_file, stop_log_to_file

# Derived class imports need to happen after ArgsFpgaDriver,
# otherwise Sphinx gets upset
from .args_ami_tool import ArgsAmi  # isort:skip

try:
    from .args_xrt import ArgsXrt
    from .xrt_info import XrtInfo

    __all__ += ("ArgsXrt", "XrtInfo")
except ModuleNotFoundError:
    # ignore failure due to missing pyxrt module
    pass
