# -*- coding: utf-8 -*-
#
# (c) 2022 CSIRO Astronomy and Space.
#
# Distributed under the terms of the CSIRO Open Source Software Licence Agreement
# See LICENSE for more info.

"""
Download FPGA FW image and register files from a remote storage.
Initially from Nexus Repository Manager and GitLab but other sources could be added.
"""

import argparse
import hashlib
import json
import os
import re
import sys
import tarfile
import time
from tempfile import mkdtemp, mkstemp
from typing import Optional
from urllib.request import urlopen

# TODO:
#  - change FileFetcher._download() to fetch the archive into a temporary
#    directory, so the subsequent invocations don't accumulate multiple
#    map files in a single place (as ArgsMap.create_from_file() uses directory
#    name rather than the full path as an argument)
#  - use a logger instead of 'print'

DEFAULT_PLATFORM_VERSION = 3
"""The default platform version e.g. when selecting XDMA 2 or XDMA 3 flavour FPGA images"""

DEFAULT_FPGA_TYPE = "u55c"
"""FPGA card type to use if unspecified"""


def get_args():
    """
    Configure command line arguments.
    """
    parser = argparse.ArgumentParser(
        description="Fetch FPGA image from a remote source (e.g. Nexus).",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "-s",
        "--source",
        help="use the specified archive source or Nexus if omitted; "
        "valid values: nexus, gitlab",
        default="nexus",
        action="store",
    )
    parser.add_argument(
        "-p",
        "--personality",
        help="image type (cnic, pst, ...)",
        action="store",
        default="cnic",
    )
    parser.add_argument(
        "-v",
        "--version",
        help="version number; use the latest version when omitted",
        action="store",
        default=None,
    )
    parser.add_argument(
        "-n",
        "--no-delete",
        help="do not delete temporary files",
        action="store_true",
    )
    parser.add_argument(
        "-x",
        "--platform-version",
        help="use the specified XDMA platform version",
        type=int,
        default=DEFAULT_PLATFORM_VERSION,
    )
    parser.add_argument(
        "-c",
        "--cache-dir",
        help="directory used as image cache",
        type=str,
        default="",
    )
    parser.add_argument("-f", "--fpga", type=str, help="FPGA card type (e.g. 'u55c')")
    return parser.parse_args()


class DownloadError(Exception):
    """A class to distinguish our download exceptions from other exceptions"""

    pass


class FileFetcher:
    personality = ("cnic", "corr", "pss", "pst", "zoom")

    def __init__(
        self,
        fw_type: str,
        version: str,
        delete_tmp: bool,
        platform_version: int,
        cache_dir: str,
        fpga_type: Optional[str] = None,
    ):
        """
        :param fw_type: firmware type (see 'personality' above)
        :param version: requested FW version (e.g. '0.2.1'); defaults to latest when empty
        :param delete_tmp: flag indicating whether to delete tarball after
        :param platform_version: platform version used to select between XDMA 2 and XDMA 3
        extracting files
        :param cache_dir: directory used for a local cache of firmware files
        :param fpga_type: FPGA card name, defaults to u55c
        """
        self._validate_fw_type(fw_type)
        self.fw_type: str = fw_type
        self.version: str = version
        self.platform_version = platform_version
        self.fpga_type = (fpga_type or DEFAULT_FPGA_TYPE).lower()
        self.candidate = []
        self.archive_name: str = ""
        self.download_url: str = ""
        self.expected_hash: str = ""
        self.hash_algorithm = hashlib.sha1  # the default algorithm
        self.image_file_path: str = ""  # set in _extract_files
        self.map_file_path: str = ""  # set in _extract_files
        self.delete_temp_file: bool = delete_tmp
        self._cache_dir = None  # directory for caching
        if os.path.isdir(cache_dir):
            self._cache_dir = cache_dir
        self._cached_file_path = None  # full path/filename of cached image

        t0 = time.time()
        if not self._get_candidates():
            raise DownloadError("didn't find candidate files")
        t1 = time.time()
        print(f"get_candidates time: {t1-t0:.6f} seconds")
        if not self._select_file():
            raise DownloadError(f"did not find file matching version {self.version}")
        t2 = time.time()
        print(f"select_file time: {t2-t1:.6f} seconds")
        self._download()
        t3 = time.time()
        print(f"download time: {t3-t2:.6f} seconds")
        # FileFetchers (e.g. Http) that ignore hash integrity introduce a new atrribute
        if getattr(self, "_skip_hash_check", None):
            pass
        elif not self._verify_hash():
            raise DownloadError(
                f"{self.archive_name} hash does not match expected one {self.expected_hash}"
            )
        t4 = time.time()
        print(f"verify_hash time: {t4-t3:.6f} seconds")
        if not self._extract_files():
            raise DownloadError(f"Did not extract files from {self.archive_name}")
        t5 = time.time()
        print(f"extract_files time {t5-t4:.6f} seconds")
        # otherwise assume all went well
        print(f"total time: {t5-t0:.6f} seconds")

    def _validate_fw_type(self, fw_type: str):
        """Confirm the specified personality is something we can handle
        otherwise throw exception as there's nothing we can do.
        """
        if fw_type not in self.personality:
            ValueError(f"Can't handle {fw_type} personality")

    def _get_candidates(self) -> bool:
        """Get possible candidate file names from the server e.g. when the
        latest version of files is needed.
        Derived class to implement.
        """
        raise NotImplementedError

    def _select_file(self) -> bool:
        """
        Select an archive matching the specified version number when
        specified, otherwise pick the latest version.
        Derived class to implement
        """
        raise NotImplementedError

    def _download(self):
        "Fetch the selected archive from a remote server (e.g. Nexus)."
        if self._cache_dir is not None:
            cached_file_path = os.path.join(self._cache_dir, self.archive_name)
            self._cached_file_path = cached_file_path
            print(f"file to be cached as {cached_file_path}")
            if os.path.isfile(cached_file_path):
                print(f"Using existing cached file '{cached_file_path}'")
                return

        dl_file_path = self.archive_name
        if self._cache_dir is not None:
            # when downloading to cache, use temp filename to avoid clashes
            print()
            fd, dl_file_path = mkstemp(dir=self._cache_dir)
            os.close(fd)

        print(f"download to filename {dl_file_path}")
        with open(dl_file_path, "bw") as out_file:
            print(f"Dowloading {self.archive_name}, please wait ... ", end="")
            out_file.write(urlopen(self.download_url).read())

        if self._cache_dir is not None:
            # after downloding for cache, rename in a single operation
            print(f"renaming {dl_file_path} -> {self._cached_file_path}")
            os.rename(dl_file_path, self._cached_file_path)

        print("done")

    def _extract_files(self) -> bool:
        """Uncompress and extract files from the archive (tarball).
        This seems to work for both Nexus and GitLab"""
        # Gets both types of firmware (xclbin, pdi) and Python (registers) file
        counter = 0
        ext_to_attr = {
            ".pdi": "image_file_path",
            ".xclbin": "image_file_path",
            ".py": "map_file_path",
        }
        if self._cached_file_path is not None:
            image_file = self._cached_file_path
            # Is there a cached version of the extracted files?
            xtracted_dir = self._cached_file_path + "_extracted"
            if os.path.isdir(xtracted_dir):
                print(f"Using existing cache of extracted files: {xtracted_dir}")
                for file in os.listdir(xtracted_dir):
                    # remove stale file from a previous session
                    if os.path.exists(file) or os.path.islink(file):
                        os.remove(file)
                    src = os.path.join(xtracted_dir, file)
                    os.symlink(src, file)
                    for extension in ext_to_attr:
                        if file.endswith(extension):
                            # TODO this is common (see below) - refactor?
                            setattr(
                                self,
                                ext_to_attr[extension],
                                os.path.abspath(file),
                            )
                            break
                return True
        else:
            image_file = self.archive_name
        with tarfile.open(image_file, mode="r:*") as arch:
            if self._cached_file_path is not None:
                # Extract files into temp dir before renaming to cache
                xtract_path = mkdtemp(dir=self._cache_dir)
            else:
                xtract_path = "."
            for fname in arch.getnames():
                for extension in ext_to_attr:
                    if fname.endswith(extension):
                        # remove stale file from a previous session:
                        if os.path.exists(fname) or os.path.islink(fname):
                            os.remove(fname)
                        print("Extracting", fname)
                        arch.extract(fname, xtract_path)
                        # TODO this is common (see above) - refactor?
                        setattr(
                            self,
                            ext_to_attr[extension],
                            os.path.abspath(fname),
                        )
                        counter += 1
                    if counter == 2:  # bail out: got both files
                        break
            if xtract_path != ".":
                # move the temporary xtracted directory to permanent cache
                os.rename(xtract_path, xtracted_dir)
                for file in os.listdir(xtracted_dir):
                    # remove stale file from a previous session:
                    if os.path.exists(file) or os.path.islink(file):
                        os.remove(file)
                    src = os.path.join(xtracted_dir, file)
                    os.symlink(src, file)

        # probably don't need tarball anymore
        if self.delete_temp_file and (self._cache_dir is None):
            os.remove(self.archive_name)
        return counter == 2

    def _verify_hash(self) -> bool:
        "Compare actual file hash against what the server says it should be."
        if hasattr(self, "_shall_verify_hash"):
            print(f"Check hash: {self._shall_verify_hash}")
            if not self._shall_verify_hash:
                return True

        if self._cached_file_path is not None:
            image_file = self._cached_file_path
        else:
            image_file = self.archive_name
        actual_hash = ""
        with open(image_file, "br") as infile:
            actual_hash = self.hash_algorithm(infile.read()).hexdigest()
        return actual_hash == self.expected_hash

    def _version_nr_supplied(self) -> bool:
        "Return True if user has supplied the version number."
        return self.version and len(self.version) > 0


class NexusFileFetcher(FileFetcher):
    """Extends FileFetcher with Nexus Repository Manager specific
    functionality"""

    KEYWORD_SEARCH_URL = (
        "https://artefact.skao.int/service/rest/v1/search?repository=raw-internal"
        "&keyword="
    )
    # zoom into the version number within the taball name which looks
    # something like: ska-low-cbf-fw-corr-u55c-0.0.7.tar.gz
    VERSION_REGEX = re.compile(r"-([\d.]+)\.tar")

    def __init__(
        self,
        fw_type: str = "cnic",
        version: str = "",
        delete_temp: bool = True,
        platform_version: int = DEFAULT_PLATFORM_VERSION,
        cache_dir: str = "",
        fpga_type: Optional[str] = None,
    ):
        fpga_type = (fpga_type or DEFAULT_FPGA_TYPE).lower()
        fpga_suffix = fpga_type
        if fpga_type == "cnic" and fpga_suffix == "u55c":
            # workaround for old CNIC builds which use "u55" (no "c")
            # should be forward-compatible with use of "u55c" as well, we hope
            fpga_suffix = "u55"
        self.url = f'{self.KEYWORD_SEARCH_URL}"ska-low-cbf-fw-{fw_type}-{fpga_suffix}*"'
        super().__init__(
            fw_type,
            version,
            delete_temp,
            platform_version,
            cache_dir,
            fpga_type,
        )

    def _sanitise_tarball_name(self, name: str) -> str:
        """Remove superfluous prefix from tarball name."""
        # 3-Dec-2024 CAR changed the API response prefixing the archive name
        #            with a slash ('/') character; drop everything up to (and
        #            including) the last occurrence of such character
        position = name.rfind("/")
        return name if position == -1 else name[position + 1 :]

    def _get_candidates(self) -> bool:
        """Get a list of candidate files matching the specified pattern from
        the remote (Nexus) host.
        Return True if we got some file names; False otherwise.
        NOTE: Nexus host returns at most 10 items per request so we'll need
              to loop around (using 'continuation token') https://is.gd/f6Wadz
        """
        cont_token = ""  # continuation token - none 1st time around
        while True:
            data = urlopen(self.url + cont_token).read()
            jsn = json.loads(data)
            for i in jsn["items"]:
                fw_name = self._sanitise_tarball_name(i["name"])
                print(f"Candidate: {fw_name}")
                self.candidate.append(
                    {
                        "name": fw_name,
                        "url": i["assets"][0]["downloadUrl"],
                        "csum": i["assets"][0]["checksum"]["sha1"],
                    }
                )
            token = jsn["continuationToken"]
            if not token:
                # bail out, no more entries
                break
            cont_token = "&continuationToken=" + token
        if len(self.candidate) == 0:
            # some hopefully helpful diagnostics
            print(f"read url: {self.url}")
            print(f"returned: {data}")
        return len(self.candidate) > 0

    # made a classmethod so we can unit test it without instantiation
    # and all the complications that arise from it
    @classmethod
    def find_exact_match(cls, packages: list, version: str) -> tuple[bool, int]:
        """Find the package with exact 'version' in a list of packages.

        :param packages: a list of dictionaries with Nexus package details
                         (name, hash, ...)
        :param version: semantic version as a string (e.g. '0.2.0')
        :return: tuple of boolean (True if exact match found) and index in the
                 list where the matching package is located
        """
        for index, pkg in enumerate(packages):
            if m := cls.VERSION_REGEX.search(pkg["name"]):
                print(f"  version {m.group(1)}")
                if m.group(1) == version:
                    return True, index
        return False, 0

    def _select_file(self) -> bool:
        """Select archive from a collection compiled in the previous step.
        If version number is specified - use that, otherwise select the
        latest version.
        """

        def use_package(package):
            "Convenience function to extract the package details."
            self.archive_name = self._sanitise_tarball_name(package["name"])
            self.expected_hash = package["csum"]
            self.download_url = package["url"]

        # NOTE caller ensures self.candidate is a non-empty list
        if self._version_nr_supplied():
            # match the specified version
            matching_candidates = [
                pkg for pkg in self.candidate if pkg["name"].find(self.version) != -1
            ]
            n_matching = len(matching_candidates)
            if n_matching == 1:
                use_package(matching_candidates[0])
            elif n_matching > 1:
                have_exact_match, index = self.find_exact_match(
                    matching_candidates, self.version
                )
                if have_exact_match:
                    use_package(matching_candidates[index])
                    print(f"  exact match {self.archive_name}")
                else:
                    # no exact match - bail out; a possible scenario:
                    # we asked for version 0.0.11 which doesn't exist but
                    # versions 0.0.118 and 0.0.119 are available
                    print(
                        f"No exact match for version {self.version}; possible candidates:"
                    )
                    for pkg in matching_candidates:
                        print(f"  {pkg['name']}")
            else:
                # no break out of loop, print hopefully helpful diagnostics
                print(f"No candidate matches version '{self.version}'")
        else:
            # pick the last entry sorted by name
            latest = sorted(self.candidate, key=lambda item: item["name"])[-1]
            self.archive_name = self._sanitise_tarball_name(latest["name"])
            self.expected_hash = latest["csum"]
            self.download_url = latest["url"]
        print("Selected: ", self.archive_name or "(none)")
        return len(self.archive_name) > 0


class GitLabFileFetcher(FileFetcher):
    """Extends FileFetcher with GitLab API for fetching package files"""

    package_url = {
        "cnic": "https://gitlab.com/api/v4/projects/ska-telescope%2flow-cbf%2fska-low-cbf-fw-cnic/packages",
        "corr": "https://gitlab.com/api/v4/projects/ska-telescope%2flow-cbf%2fska-low-cbf-fw-corr/packages",
        "pss": "https://gitlab.com/api/v4/projects/ska-telescope%2flow-cbf%2fska-low-cbf-fw-pss/packages",
        "pst": "https://gitlab.com/api/v4/projects/ska-telescope%2flow-cbf%2fska-low-cbf-fw-pst/packages",
        "zoom": "https://gitlab.com/api/v4/projects/ska-telescope%2flow-cbf%2fska-low-cbf-fw-zoom/packages",
    }
    """Maps FW type to the URL where its package could be found"""

    file_url = {
        "cnic": "https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-fw-cnic/-/package_files/%d/download",
        "corr": "https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-fw-corr/-/package_files/%d/download",
        "pss": "https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-fw-pss/-/package_files/%d/download",
        "pst": "https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-fw-pst/-/package_files/%d/download",
        "zoom": "https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-fw-zoom/-/package_files/%d/download",
    }
    """Maps FW type to the URL where its file could be found"""

    def __init__(
        self,
        fw_type: str = "cnic",
        version: str = "",
        delete_temp: bool = True,
        platform_version: int = DEFAULT_PLATFORM_VERSION,
        cache_dir: str = "",
        fpga_type: Optional[str] = None,
    ):
        self._validate_fw_type(fw_type)
        self._pkg_url = self.package_url[fw_type]
        fpga_type = (fpga_type or DEFAULT_FPGA_TYPE).lower()
        map_fw_type_to_fname = {
            "cnic": f"cnic_{fpga_type}",
            "corr": f"correlator_{fpga_type}",
            "pss": f"pss_{fpga_type}",
            "pst": f"pst_{fpga_type}",
            "zoom": f"zoom_{fpga_type}",
        }
        """Maps FW type to the file name pattern to look for."""
        self._fname_pattern = map_fw_type_to_fname[fw_type]
        if cache_dir == "":
            self._cache_dir = None
        else:
            self._cache_dir = cache_dir
        super().__init__(
            fw_type,
            version,
            delete_temp,
            platform_version,
            cache_dir,
            fpga_type,
        )

    def _get_candidates(self) -> bool:
        """Get the list of packages on GitLab server. By default API will
        return 20 packages, see https://docs.gitlab.com/ee/api/packages.html#list-package-files
        """
        # GitLab will list at most this number of packages per page:
        MAX_PKG_PER_PAGE = 100
        data = urlopen(
            f"{self._pkg_url}?per_page={MAX_PKG_PER_PAGE}&pagination=keyset&order_by=created_at&sort=desc"
        ).read()
        self.candidate = json.loads(data)
        count = len(self.candidate)
        if count >= MAX_PKG_PER_PAGE:
            print(f"\n\tWARNING: got {count} candidates; please remove old packages\n")
        return count > 0

    def _select_file(self) -> bool:
        """Select archive from a collection compiled in the previous step.
        If version number is specified - use that, otherwise select the
        latest version.
        """

        def pick_artefact(package_id):
            """Helper function to pick between e.g. U50 and U55 flavours; also
            ensures a matching platform (Xilinx 'shell') version is used"""
            for artefact in self._get_artefacts(package_id):
                fname = artefact["file_name"]
                print(f"Candidate  {fname}")
                if not fname.startswith(self._fname_pattern):
                    continue
                if self.fpga_type.startswith("u"):
                    # Only relevant for FPGAs that use XRT
                    matching_platform = f"xdma_{self.platform_version}" in fname or (
                        # also allow old style name without platform version
                        # (was only used with platform v2)
                        self.platform_version == 2
                        and "xdma_" not in fname
                    )
                    if not matching_platform:
                        continue
                file_id = artefact["id"]
                self.archive_name = fname
                self.expected_hash = artefact["file_sha256"]
                self.download_url = self._get_tarball_url(file_id)
                self.hash_algorithm = hashlib.sha256
                break

        def exact_match_id():
            """'From the list of matching candidates pick the package with
            exact version match (if it exists) and return the package ID.
            Otherwise return None."""

            for candidate in matching_candidates:
                if self.version == candidate["version"]:
                    return candidate["id"]
            return None

        # NOTE caller ensures self.candidate is a non-empty list
        if self._version_nr_supplied():
            matching_candidates = [
                pkg for pkg in self.candidate if pkg["version"].find(self.version) != -1
            ]
            if len(matching_candidates) > 0:
                id = exact_match_id() or matching_candidates[0]["id"]
                pick_artefact(id)
        else:
            # the list of candidates is sorted (descending creation time order)
            package_id = self.candidate[0]["id"]
            pick_artefact(package_id)
        return len(self.archive_name) > 0

    def _get_artefacts(self, package_id):
        "Return a collection of artefacts belonging to the package"
        url = self._pkg_url + f"/{package_id}/package_files"
        data = urlopen(url).read()
        return json.loads(data)

    def _get_tarball_url(self, file_id: int) -> str:
        """Get the URL where we can fetch the tarball with the specified ID
        for current FW type (cnic, pst, etc)
        """
        return self.file_url[self.fw_type] % file_id


class HttpFileFetcher(FileFetcher):
    """Extends FileFetcher with plain old HTTP(S) server fetch"""

    def __init__(
        self,
        fw_type: str = "cnic",
        version: str = "",
        delete_temp: bool = True,
        platform_version: int = DEFAULT_PLATFORM_VERSION,
        cache_dir: str = "",
        fpga_type: Optional[str] = None,
    ):
        fpga_type = (fpga_type or DEFAULT_FPGA_TYPE).lower()
        self.url = version
        print(f"URL: {version}")
        self._skip_hash_check = True
        super().__init__(
            fw_type,
            version,
            delete_temp,
            platform_version,
            cache_dir,
            fpga_type,
        )

    def _get_candidates(self) -> bool:
        """
        The required file is specified in the URL as in
         http://202.9.15.140:8080/bb/pst_u55c_gen3x16_xdma_3_1.0.1-main.d77c5121-vitis.2022.2.tar.xz
        but go through the motions orchestrated by the base class.
        """
        self.candidate.append(self.url)
        return len(self.candidate) > 0

    def _select_file(self) -> bool:
        """Extract tarball name from the full URL"""
        last_slash = self.url.rfind("/")
        self.archive_name = self.url[last_slash + 1 :]
        self.download_url = self.url
        print("Selected package: ", self.archive_name, " from URL", self.download_url)
        return len(self.archive_name) > 0


def get_file_fetcher(args) -> FileFetcher:
    "Get a class object of specified type"
    name_to_class_map = {
        "nexus": NexusFileFetcher,
        "gitlab": GitLabFileFetcher,
    }

    cli_caller = not isinstance(args, str)
    source = args.source if cli_caller else args

    if source in name_to_class_map:
        return name_to_class_map[source]
    # check if HTTP(S) downloader is required
    if re.match(r"http.?://", source):
        if cli_caller:
            # copy the URL (already containing the required FW version)
            # to args.version field which will be picked up by __init__
            args.version = args.source
        return HttpFileFetcher
    raise ValueError(f"invalid type {source}")


def download():
    """Handle command line arguments and instantiate the class responsible
    for fetching files.
    """
    args = get_args()
    fetcher = get_file_fetcher(args)
    try:
        dl = fetcher(
            args.personality,
            args.version,
            not args.no_delete,
            args.platform_version,
            args.cache_dir,
            args.fpga,
        )
    except DownloadError as ex:  # Any download exception is fatal
        print(f"Error: {ex}")
        return 1

    print(f"{dl.archive_name} ok")
    return 0


if __name__ == "__main__":
    sys.exit(download())
