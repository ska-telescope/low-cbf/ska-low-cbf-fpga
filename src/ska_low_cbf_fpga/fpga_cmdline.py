# -*- coding: utf-8 -*-
#
# (c) 2021 CSIRO Astronomy and Space.
#
# Distributed under the terms of the CSIRO Open Source Software Licence Agreement
# See LICENSE for more info.

"""
Xilinx Alveo FPGA acclerator card command-line ARGS register access utility.
"""

import argparse
import logging
from collections import defaultdict, namedtuple
from time import localtime, strftime, time

from blessed import Terminal
from IPython import start_ipython
from IPython.core.getipython import get_ipython
from traitlets.config import Config

import ska_low_cbf_fpga.register_load as register_load
from ska_low_cbf_fpga.args_fpga import ArgsFpgaDriver, mem_parse, str_from_int_bytes
from ska_low_cbf_fpga.args_map import ArgsMap
from ska_low_cbf_fpga.driver import create_driver_map_info
from ska_low_cbf_fpga.fpga_icl import FpgaPersonality, IclField, IclFpgaField


class UserInterface:
    TOP_ROW_OFFSET = 3  # note heading is printed 2 rows above top of table
    LEFT_OFFSET = 3
    TABLE_ROWS = 12
    WriteLogEntry = namedtuple(
        "WriteLogEntry", ["time", "peripheral", "register", "value"]
    )
    HELP_KEYS = {
        "H": "Display Help",
        "Q": "Quit",
        "X": "Toggle Hexadecimal Display",
        "Up/Down": "Select FPGA register",
        "Enter": "Write a value to FPGA register",
    }

    def __init__(self, fpga, card, kernel):
        self.term = Terminal()
        self.fpga = fpga
        self.card = card
        self.kernel = kernel
        self.cur_x = 0
        self.cur_y = 0
        self.write_log = []
        self.max_name_len = max(
            [
                len(reg_name)
                for peri in fpga.peripherals
                for reg_name in fpga[peri]._fields.keys()
            ]
        )
        self.max_help_key_len = max(len(key) for key in self.HELP_KEYS.keys())
        self.max_help_dsc_len = max(len(val) for val in self.HELP_KEYS.values())
        # in future _rows/cols could be modified for multi-column display
        self.n_rows = sum([len(fpga[peri]._fields.keys()) for peri in fpga.peripherals])
        self.n_cols = 1
        self.cache = defaultdict(lambda: None)
        self.table_bottom = None
        self.entry_val = ""
        self.entry_mode = False
        self.hex_mode = False
        self.help_mode = False
        self.loop = False
        self.selected = (None, None)  # (peripheral, register [, index])
        self.fresh = defaultdict(lambda: False)

    def render_loop(self):
        self.loop = True

        with self.term.fullscreen(), self.term.cbreak(), self.term.hidden_cursor():
            while self.loop:
                self.update_cache()
                self.prune_log(20)

                print(self.term.clear)
                self.render_headline()
                self.render_registers()
                self.render_log()
                if self.help_mode:
                    self.render_help()
                if self.entry_mode:
                    self.render_entry()
                self.render_status()

                self.process_input(self.term.inkey(timeout=5))

    def update_cache(self):
        for peri in self.fpga.peripherals:
            for name, reg in self.fpga[peri]._fields.items():
                value = reg.value
                # TODO new "fresh" logic
                # if isinstance(reg, FpgaRam):
                #    self.fresh[name] = any(value != self.cache[name])
                # elif isinstance(reg, FpgaRegister):
                #    self.fresh[name] = value != self.cache[name]
                # else:
                #    raise NotImplementedError("Unknown register data type")
                self.cache[name] = value

    def prune_log(self, timeout):
        # remove stale log entries
        while self.write_log and self.write_log[-1].time < time() - timeout:
            self.write_log.pop()

    def process_input(self, key):
        term = self.term
        if key:
            if self.entry_mode:
                # filter out multi-byte sequences & control characters
                if not key.is_sequence and ord(key) > ord(" "):
                    self.entry_val += key
                elif key.code == term.KEY_BACKSPACE:
                    self.entry_val = self.entry_val[:-1]
                elif key.code == term.KEY_ENTER:
                    self.entry_mode = False
                    peri = self.selected[0]
                    reg = self.selected[1]
                    try:
                        index = self.selected[2]
                    except IndexError:
                        # this implies we have an FpgaRegister, not FpgaRam
                        index = None
                    type_ = self.fpga[peri][reg].type
                    if type_ == int:
                        # base 0 means detect base from string (0x, 0b)
                        write_val = int(self.entry_val, base=0)
                    else:
                        write_val = self.entry_val
                    if index is None:
                        self.fpga[peri][reg] = write_val
                        log_reg = reg
                    else:
                        # FpgaRam
                        self.fpga[peri][reg][index] = write_val
                        log_reg = f"{peri}[{index}]"

                    self.write_log.insert(
                        0,
                        UserInterface.WriteLogEntry(
                            time(), reg, log_reg, self.entry_val
                        ),
                    )
                # clean-up when finished with entry
                if not self.entry_mode:
                    self.entry_val = ""
            else:
                if key.code == term.KEY_ENTER:
                    self.help_mode = False
                    self.entry_mode = True
                    if self.hex_mode:
                        self.entry_val = "0x"
                if key.code == term.KEY_UP and self.cur_y > 0:
                    self.cur_y -= 1
                elif key.code == term.KEY_DOWN and self.cur_y < self.n_rows - 1:
                    self.cur_y += 1
                elif key.code == term.KEY_LEFT and self.cur_x > 0:
                    self.cur_x -= 1
                elif key.code == term.KEY_RIGHT and self.cur_x < self.n_cols - 1:
                    self.cur_x += 1
                elif key.lower() == "q":
                    self.loop = False
                elif key.lower() == "x":
                    self.hex_mode = not self.hex_mode
                elif key.lower() == "h":
                    self.help_mode = not self.help_mode
            if key.code == term.KEY_ESCAPE:
                self.help_mode = False
                self.entry_mode = False

    def render_headline(self):
        heading = self.term.ljust(
            "FPGA Registers", width=self.term.width // 2 - self.LEFT_OFFSET
        )
        if self.write_log:
            heading += "Write Log"
        with self.term.location(x=self.LEFT_OFFSET, y=self.TOP_ROW_OFFSET - 2):
            print(self.term.cyan(heading))

    def render_registers(self):
        term = self.term
        row = 0
        row_min = min(max(self.cur_y - 2, 0), self.n_rows - self.TABLE_ROWS - 1)
        row_max = self.TABLE_ROWS + row_min

        for peri in self.fpga.peripherals:
            for name, reg in self.fpga[peri]._fields.items():
                if row < row_min:
                    row += 1
                    continue
                if row > row_max:
                    break
                selected = row == self.cur_y
                y_pos = row + self.TOP_ROW_OFFSET - row_min
                fresh = self.fresh[name]
                if selected:
                    self.selected = (peri, name)
                value = reg.value
                with term.location(x=self.LEFT_OFFSET, y=y_pos):
                    if fresh and selected:
                        print(term.bold_black_on_yellow(name))
                    elif selected:
                        print(term.black_on_white(name))
                    elif fresh:
                        print(term.bold_yellow(name))
                    else:
                        print(name)
                with term.location(x=self.max_name_len + self.LEFT_OFFSET + 1, y=y_pos):
                    if self.hex_mode:
                        value = hex(value)
                    else:
                        value = str(value)
                    if fresh:
                        print(term.bold_yellow(value))
                    else:
                        print(value)
                if selected and self.entry_mode:
                    with term.location(x=0, y=y_pos):
                        print(term.bold_red("*"))
                    with term.location(
                        x=self.max_name_len + len(str(value)) + 5, y=y_pos
                    ):
                        print(term.bold_red("*"))
                row += 1
        self.table_bottom = row

    def render_log(self):
        term = self.term
        x_log = self.term.width // 2
        y_pos = self.TOP_ROW_OFFSET
        for w in self.write_log:
            with term.location(x=x_log, y=y_pos):
                print(
                    term.dimgrey(
                        "{} {}.{} <= {}".format(
                            strftime("%H:%M:%S", localtime(w.time)),
                            w.peripheral,
                            w.register,
                            w.value,
                        )
                    )
                )
            y_pos += 1

    def render_status(self):
        term = self.term
        with term.location(x=0, y=term.height - 3):
            print(
                f"{term.dimgrey}Card: {term.grey}{self.card:2}{term.dimgrey}"
                f" Kernel: {self.kernel}"
            )
        with term.location(x=0, y=term.height - 2):
            timestr = term.dimgrey(strftime("%H:%M:%S", localtime()))
            if self.entry_mode:
                keystr = term.ljust(
                    term.cyan_bold("(Esc): Cancel"), width=term.width // 2
                )
            else:
                keystr = term.ljust(
                    term.cyan_bold("(Q)uit (H)elp"), width=term.width // 2
                )
            print(keystr + timestr)

    def render_entry(self):
        # new value entry prompt
        term = self.term
        y_pos = self.table_bottom + self.TOP_ROW_OFFSET + 2
        with term.location(x=2, y=y_pos):
            if len(self.selected) == 2:
                # FpgaRegister
                print(
                    f"{term.bold_cyan}Set {self.selected[0]}.{self.selected[1]}:"
                    f" {term.normal}{self.entry_val}"
                )
            elif len(self.selected) == 3:
                # FpgaRam
                print(
                    f"{term.bold_cyan}Set {self.selected[0]}.{self.selected[1]}"
                    f"[{self.selected[2]}]: {term.normal}{self.entry_val}"
                )

    def render_help(self):
        term = self.term
        y_pos = (term.height // 2) - (len(self.HELP_KEYS) // 2)
        x_pos = (term.width // 2) - (
            self.max_help_dsc_len + self.max_help_key_len + 1
        ) // 2
        for key, desc in self.HELP_KEYS.items():
            with term.location(y=y_pos, x=x_pos):
                key = term.center(key, self.max_help_key_len)
                print(f"{term.bold_cyan}{key} {term.normal}{desc}")
                y_pos += 1


def create_drivers(args, logger) -> {str: (ArgsFpgaDriver, ArgsMap)}:
    """
    Create the required driver object(s)
    :param args: arguments from argparse
    :param logger: for status output
    :return dict mapping BDF to driver object & map directory
    """
    if args.firmware_paths and args.simulate:
        raise NotImplementedError(
            "Combination of simulation & real devices not supported"
        )

    logger.info(f"Cards: {args.devices}")
    if args.firmware_paths:
        if len(args.firmware_paths) == 1:
            logger.info("Firmware: " + args.firmware_paths[0])
        else:
            logger.info("Firmware files: " + str(args.firmware_paths))
        logger.info("PCIe device(s): " + str(args.devices))

    memory_configs = []
    if args.memories:
        for memory_config_string in args.memories:
            memory = mem_parse(memory_config_string)
            mem_info = "Memories: "
            for m in memory:
                mem_info += str_from_int_bytes(m.size)
                mem_info += " shared" if m.shared else " internal"
                mem_info += ", "
            logger.info(mem_info[:-2])
            memory_configs.append(memory)
    # pad memory configs so zip works later
    memory_configs += [None] * (len(args.devices) - len(memory_configs))
    if args.simulate:
        memory_configs += [None] * (len(args.simulate) - len(memory_configs))

    devices = {}
    if args.simulate:
        for n, (simulation_map, memory) in enumerate(
            zip(args.simulate, memory_configs)
        ):
            logger.info("Simulating: " + simulation_map)
            devices[n] = create_driver_map_info(
                fpgamap_path=simulation_map, mem_config=memory
            )
    else:
        if not args.firmware_paths:
            raise RuntimeError("Please supply either kernel or simulation map")

        if not len(args.devices) == len(args.firmware_paths):
            raise RuntimeError("Please supply the same number of devices & kernels")

        for device, firmware_path, memory in zip(
            args.devices, args.firmware_paths, memory_configs
        ):
            devices[device] = create_driver_map_info(
                firmware_path=firmware_path,
                mem_config=memory,
                device=device,
                logger=logger,
            )

    return devices


def create_logger(
    verbose: int, quiet: int, console: bool, interactive: bool
) -> logging.Logger:
    """
    Create a logger object with appropriate level & output configuration.

    :param verbose: higher number, more output.
    :param quiet: overrides verbose. higher number, less output.
    :param console: debug console in use (more verbose)
    :param interactive: interactive UI in use (log to file)
    """
    level = logging.WARNING  # default log level
    net_verbosity = verbose + console - quiet
    if net_verbosity < -2:
        level = logging.CRITICAL + 1
    elif net_verbosity == -2:
        level = logging.CRITICAL
    elif net_verbosity == -1:
        level = logging.ERROR
    elif net_verbosity == 1:
        level = logging.INFO
    elif net_verbosity >= 2:
        level = logging.DEBUG

    logger = logging.getLogger()
    logger.setLevel(level)

    if interactive:
        fh = logging.FileHandler("fpgacmdline.log")
        formatter = logging.Formatter("%(asctime)s:%(levelname)s:%(message)s")
        fh.setFormatter(formatter)
        logger.addHandler(fh)
    else:
        sh = logging.StreamHandler()
        formatter = logging.Formatter("%(message)s")
        sh.setFormatter(formatter)
        logger.addHandler(sh)

    return logger


class FpgaCmdline:
    print_as_table: bool = True  # default to table format

    def __init__(
        self,
        personality: type = FpgaPersonality,
        personality_map: dict = {},
    ):
        """
        FPGA Command Line Utility Class

        :param personality: default personality ICL class to use
        :param personality_map: dictionary mapping FPGA firmware personality
        (4 character code) to ICL class. e.g. {"CNIC": CnicFpga}
        """
        self.parser = argparse.ArgumentParser(
            description="ska-low-cbf FPGA Command-Line Utility"
        )
        self.configure_parser()
        args = self.parser.parse_args()
        self.args = args
        self.logger = create_logger(
            args.verbose,
            args.quiet,
            args.console,
            args.interactive,
        )
        self.drivers = create_drivers(self.args, self.logger)
        self._personality_map = personality_map
        """FPGA personality ID to ICL class map"""

        self.personality_args = dict()
        # call hook for derived personality-specific command line utilities
        self.set_personality_args()

        self.fpgas = {}
        """dict mapping device (BDF) to FpgaPersonality object instance"""

        for device, (driver, map_, info) in self.drivers.items():
            self.logger.debug(f"Creating default ICL object for {device}")
            # use a default personality object to read the firmware ID
            fpga = personality(
                driver=driver,
                map_=map_,
                logger=self.logger,
                hardware_info=info,
                **self.personality_args,
            )
            fw_personality = fpga.fw_personality.value
            if (
                fw_personality in self._personality_map
                and self._personality_map[fw_personality] != personality
            ):
                del fpga
                new_type = self._personality_map[fw_personality]
                self.logger.info(f"Creating {type(new_type)} object " f"for {device}")
                fpga = new_type(
                    driver=driver,
                    map_=map_,
                    logger=self.logger,
                    hardware_info=info,
                    **self.personality_args,
                )
            self.fpgas[device] = fpga

        self.run()

    def configure_parser(self) -> None:
        """
        Configure our base command line arguments.
        Derived classes can extend/replace as appropriate.

        Command Line Arguments are designed to be (mostly) compatible with
        alveo2gemini, but we can't use -h for HBM (built in, used for help),
        so -m for memory instead.
        """

        self.parser.add_argument(
            "-f",
            "--firmware",
            dest="firmware_paths",
            type=str,
            help="path to xclbin kernel or pdi image file(s)",
            nargs="+",
        )
        self.parser.add_argument(
            "-d",
            "--device",  # device(s) - named for consistency with Xilinx tools
            dest="devices",
            default=["0"],  # get the first device by default
            type=str,
            help="Bus:Device.Function PCIe addresses of card(s) to use",
            nargs="+",
        )
        self.parser.add_argument(
            "-m",
            "--memory",
            dest="memories",
            type=str,
            help="""HBM configuration(s) per card - <size><unit><s|i>.
                     size: int
                     unit: k, M, G (powers of 1024)
                     s: shared
                     i: FPGA internal
                     e.g. '128Ms:1Gi'""",
            nargs="+",
        )
        self.parser.add_argument(
            "-s",
            "--simulate",
            type=str,
            help="path(s) to fpgamap_nnnnnnnn.py file(s) to simulate",
            nargs="+",
        )
        self.parser.add_argument(
            "-r",
            "--registers",
            type=argparse.FileType(mode="r"),
            help="register setting text file to load",
        )
        self.parser.add_argument(
            "-i",
            "--interactive",
            action="store_true",
            help="use interactive interface (unsupported!)",
        )
        self.parser.add_argument(
            "-c",
            "--console",
            action="store_true",
            help="use IPython console, implies --verbose",
        )
        self.parser.add_argument(
            "-e",
            "--exec",
            type=argparse.FileType(mode="r"),
            help="Python file to execute",
        )
        self.parser.add_argument(
            "-v",
            "--verbose",
            action="count",
            default=0,
            help="verbose (1x: info, 2x: debug)",
        )
        self.parser.add_argument(
            "-q",
            "--quiet",
            action="count",
            default=0,
            help="quiet (1x: error, 2x: critical, 3x: nothing)",
        )

    def set_personality_args(self) -> None:
        """
        Derived classes can overload this to set items in personality_args
        as needed, they will then be passed to the personality's constructor
        """
        pass

    @staticmethod
    def print_tbl(inst, p, cycle):
        'Function to "pretty print" FPGA registers in tabular format'
        if cycle:
            p.text("oops, found cycle")
        else:
            keys = inst.__dict__.keys()
            width = max([len(x) for x in keys])
            line_format = "{k:{width}} : {v}\n"
            # NOTE: 'value' attribute is 'property' - not in the __dict__
            t = line_format.format(k="value", width=width, v=inst.value)
            for k in keys:
                t += line_format.format(k=k, width=width, v=inst.__dict__[k])
            p.text(t)

    @staticmethod
    def print_std(inst, p, cycle):
        "Standard representation of FPGA registers"
        p.text("oops, found cycle" if cycle else str(inst))

    def run(self) -> None:
        """
        Launch the user-requested action(s)
        """
        # local variables for user convenience in exec or console
        # (ignore unused variable errors from flake8)
        drivers = self.drivers  # noqa: F841
        fpgas = self.fpgas  # noqa: F841
        if len(self.args.devices) == 1:
            first_device = self.args.devices[0]
            if self.args.simulate:
                first_device = 0
            fpga = self.fpgas[first_device]  # noqa: F841
            driver = self.drivers[first_device]  # noqa: F841

        if self.args.registers:
            for fpga in self.fpgas.values():
                register_load.load(fpga, self.args.registers.read())
        if self.args.exec and not self.args.console:
            # this has strange results when we get to IPython
            # e.g. a function defined in the file cannot see other functions
            # that are also defined in the file
            # so we use a different scheme for console mode
            exec(self.args.exec.read())
        if self.args.interactive:
            if len(self.args.devices) > 1:
                raise NotImplementedError(
                    "Only 1 FPGA card supported in interactive mode."
                )
            card = self.args.devices[0]
            ui = UserInterface(self.fpgas[card], card, self.args.firmware_paths[0])
            ui.render_loop()
        elif self.args.console:
            cfg = Config()
            # execute these lines on IPyton shell startup:
            cfg.InteractiveShellApp.exec_lines = [
                'fm=get_ipython().display_formatter.formatters["text/plain"]',
                "_=fm.for_type( IclField, FpgaCmdline.print_tbl )",
                "_=fm.for_type( IclFpgaField, FpgaCmdline.print_tbl )",
                'print("INFO: done with registering callback")',
            ]
            if self.args.exec:
                cfg.InteractiveShellApp.exec_lines.append(
                    f"print('Executing {self.args.exec.name}')"
                )
                # %run options:
                # i: use IPython namespace
                #    (so scripts can access the 'fpga' object etc.)
                # n: __name__ NOT set to "__main__"
                #    (for consistency with non-console mode)
                cfg.InteractiveShellApp.exec_lines.append(
                    f"%run -in {self.args.exec.name}"
                )

            # unlike embed(), start_ipython() does not capture the caller's
            # namespace, so we'll supply it manually
            ns = locals()
            ns.update(globals())
            start_ipython(argv=[""], config=cfg, user_ns=ns)


def toggle() -> None:
    """
    Intended for use from within IPython shell; toggle between the plain and
    tabular data format.
    """
    fm = get_ipython().display_formatter.formatters["text/plain"]
    for typ in (IclField, IclFpgaField):
        fm.for_type(
            typ,
            FpgaCmdline.print_tbl
            if not FpgaCmdline.print_as_table
            else FpgaCmdline.print_std,
        )
    FpgaCmdline.print_as_table = not FpgaCmdline.print_as_table
    print(f"print as table: {FpgaCmdline.print_as_table}")


def main():
    FpgaCmdline()


if __name__ == "__main__":
    main()
