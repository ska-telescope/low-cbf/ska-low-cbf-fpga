# -*- coding: utf-8 -*-
"""Release information for ska_low_cbf_fpga"""

name = """ska_low_cbf_fpga"""
version = "0.19.9"
version_info = version.split(".")
description = """SKA Low CBF FPGA Software Interface"""
author = "CSIRO"
author_email = "andrew dot bolin at csiro dot au"
license = """CSIRO Open Source Software Licence Agreement"""
url = """https://www.skatelescope.org/"""
copyright = """CSIRO"""
