# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence Agreement
# See LICENSE.txt for more info.

import numpy as np
import pytest

from ska_low_cbf_fpga.args_fpga import mem_parse
from ska_low_cbf_fpga.args_simulator import ArgsSimulator


class TestArgsSimulatorInit:
    def test_create(self, simulator_map_path):
        ArgsSimulator.create_from_file(simulator_map_path)

    def test_create_with_mem(self, simulator_map_path):
        memory = mem_parse("2Ms:2Gi:2Ms")
        ArgsSimulator.create_from_file(
            fpgamap_path=simulator_map_path, mem_config=memory
        )


@pytest.mark.usefixtures("simulator")
class TestArgsSimulatorRegisters:
    def test_build(self, simulator):
        assert simulator.get_map_build() == 0x12345678

    def test_write_array(self, simulator):
        values = np.array([1, 2, 3, 4], dtype=np.uint32)
        simulator.write(8, values)
        np.testing.assert_array_equal(simulator.read(8, values.size), values)


@pytest.mark.parametrize("mem_index", [1, 3])
@pytest.mark.usefixtures("simulator")
class TestArgsSimulatorMemory:
    def test_read_memory(self, simulator, mem_index):
        read = simulator.read_memory(mem_index)
        # ugly way to find the desired size, but the main thing we're testing
        # here is that read_memory returns something plausible so...
        assert read.nbytes == simulator._mem_config[mem_index].size

    def test_write_full_memory(self, simulator, mem_index):
        size = simulator.read_memory(mem_index).nbytes
        # write some random numbers
        write_data = np.random.randint(255, size=size, dtype=np.uint8)
        simulator.write_memory(mem_index, write_data)
        read_data = simulator.read_memory(mem_index).view(dtype=np.uint8)
        assert np.array_equal(write_data, read_data)

    def test_write_memory_offset(self, simulator, mem_index):
        # zero-out the memory
        z = np.zeros(simulator.read_memory(mem_index).nbytes, dtype=np.uint8)
        simulator.write_memory(mem_index, z)
        # write some random numbers with an offset
        offset = 16
        size = 32
        write_data = np.random.randint(255, size=size, dtype=np.uint8)
        simulator.write_memory(mem_index, values=write_data, offset_bytes=offset)
        # read back whole memory
        read_data = simulator.read_memory(mem_index).view(dtype=np.uint8)
        # compare region of interest
        assert np.array_equal(write_data, read_data[offset : offset + size])
        # ensure zeros elsewhere
        assert np.count_nonzero(read_data[:offset]) == 0
        assert np.count_nonzero(read_data[offset + size :]) == 0

    def test_read_memory_offset(self, simulator, mem_index):
        # write some random numbers with an offset
        offset = 16
        size = 32
        write_data = np.random.randint(255, size=size, dtype=np.uint8)
        simulator.write_memory(mem_index, values=write_data, offset_bytes=offset)
        # read back region of interest using offset & size
        read_data = simulator.read_memory(
            mem_index, offset_bytes=offset, size_bytes=size
        ).view(dtype=np.uint8)
        assert np.array_equal(write_data, read_data)


@pytest.mark.usefixtures("simulator")
class TestArgsSimulatorNotSharedMemory:
    def test_read_not_shared_memory(self, simulator):
        # don't care what exception it raises, as long as it raises one
        with pytest.raises(Exception):
            simulator.read_memory(2)

    def test_write_not_shared_memory(self, simulator):
        # don't care what exception it raises, as long as it raises one
        with pytest.raises(Exception):
            simulator.write_memory(2)
