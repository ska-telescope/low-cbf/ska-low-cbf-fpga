# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence Agreement
# See LICENSE.txt for more info.
import os

from ska_low_cbf_fpga.args_fpga import ARGS_MAGIC

my_dir = os.path.dirname(os.path.abspath(__file__))
sim_file = os.path.join(my_dir, "fpgamap_12345678.py")


class TestCmdLine:
    def test_help(self):
        """Make sure our command is registered by using the help flag"""
        # check for zero exit code
        assert os.system("fpga -h") == 0

    def test_sim(self):
        """Check that simulation of our test map works"""
        assert os.system(f"fpga --simulate '{sim_file}'") == 0

    def test_exec(self):
        """Confirm that we can execute our test script"""
        exec_file = os.path.join(my_dir, "exec_get_magic_number.py")
        command = f"fpga --simulate '{sim_file}' --exec '{exec_file}'"
        assert os.system(command) == 0
        output = os.popen(command)
        assert int(output.read()) == ARGS_MAGIC

    def test_multi_sim(self):
        """Multiple simulated FPGAs"""
        command = "fpga" + f" --simulate '{sim_file}'" * 3
        assert os.system(command) == 0

    def test_customised(self):
        """Check that we can customise the CLI downstream"""
        command = (
            f"python3 {os.path.join(my_dir, 'cmdline_custom.py')} " f"-s '{sim_file}'"
        )
        assert os.system(command) == 0
