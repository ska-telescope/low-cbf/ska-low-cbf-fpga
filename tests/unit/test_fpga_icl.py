# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence Agreement
# See LICENSE.txt for more info.
import os
from unittest.mock import Mock, patch

import numpy as np
import pytest

from ska_low_cbf_fpga import ArgsFpgaDriver
from ska_low_cbf_fpga.args_fpga import WORD_SIZE, ArgsWordType
from ska_low_cbf_fpga.fpga_icl import (
    DISCOVER_ALL,
    DISCOVER_PROPERTIES,
    DISCOVER_REGISTERS,
    FpgaPeripheral,
    FpgaPersonality,
    IclField,
    IclFpgaField,
)

from .conftest import DummyArgsMap, my_fields


@pytest.fixture
def icl_test_int_field():
    yield IclField[int](description="Testing", value=42, type_=int)


@pytest.fixture
def icl_test_fpga_field():
    class FakeDriver(ArgsFpgaDriver):
        def __init__(self, *args, **kwargs):
            self.x = 0

        def read(self, source, length=None):
            return self.x

        def write(self, destination, values):
            self.x = values

        def read_memory(self, index: int) -> np.ndarray:
            pass

        def write_memory(self, index: int, values: np.ndarray, offset: int = 0):
            pass

        def _setup(self, *args, **kwargs):
            """Not used in tests"""
            raise NotImplementedError

        def _load_firmware(self):
            """Not used in tests"""
            raise NotImplementedError

        def _init_buffers(self):
            """Not used in tests"""
            raise NotImplementedError

    fake_driver = FakeDriver()
    field = IclFpgaField(
        description="Testing",
        driver=fake_driver,
    )
    field.value = 42
    yield field


@pytest.fixture
def icl_test_fpga_multibit_field():
    class FakeDriver(ArgsFpgaDriver):
        def __init__(self, *args, **kwargs):
            self.x = 0

        def read(self, source, length=None):
            return self.x

        def write(self, destination, values):
            self.x = values

        def read_memory(self, index: int) -> np.ndarray:
            pass

        def write_memory(self, index: int, values: np.ndarray, offset: int = 0):
            pass

        def _setup(self, *args, **kwargs):
            """Not used in tests"""
            raise NotImplementedError

        def _load_firmware(self):
            """Not used in tests"""
            raise NotImplementedError

        def _init_buffers(self):
            """Not used in tests"""
            raise NotImplementedError

    fake_driver = FakeDriver()
    field = IclFpgaField(
        description="Testing",
        driver=fake_driver,
        bit_offset=4,
        width=6,
    )
    field.value = 42
    yield field


@pytest.fixture
def icl_test_fpga_array_field():
    class FakeDriver(ArgsFpgaDriver):
        def __init__(self, *args, **kwargs):
            self.x = np.array([0, 1, 2, 3], dtype=ArgsWordType)

        def read(self, source, length=None):
            start = source // WORD_SIZE
            if length is None:
                return self.x[start]
            else:
                return self.x[start : start + length]

        def write(self, destination, values):
            new_values = np.array(values, ndmin=1, dtype=ArgsWordType)
            start = destination // WORD_SIZE
            end = start + len(new_values)
            self.x[start:end] = new_values

        def read_memory(self, index: int) -> np.ndarray:
            pass

        def write_memory(self, index: int, values: np.ndarray, offset: int = 0):
            pass

        def _setup(self, *args, **kwargs):
            """Not used in tests"""
            raise NotImplementedError

        def _load_firmware(self):
            """Not used in tests"""
            raise NotImplementedError

        def _init_buffers(self):
            """Not used in tests"""
            raise NotImplementedError

    fake_driver = FakeDriver()
    field = IclFpgaField(
        description="Testing",
        driver=fake_driver,
        address=0,
        length=4,
    )
    yield field


class TestIclField:
    @pytest.mark.usefixtures("icl_test_int_field")
    def test_int_cast(self, icl_test_int_field):
        assert int(icl_test_int_field) == 42

    @pytest.mark.usefixtures("icl_test_int_field")
    def test_integer_arithmetic(self, icl_test_int_field):
        assert icl_test_int_field + 1 == 43
        assert icl_test_int_field - 1 == 41
        assert icl_test_int_field * 2 == 84
        assert icl_test_int_field**2 == 1764
        assert icl_test_int_field / 5 == 8.4
        assert icl_test_int_field // 5 == 8
        assert icl_test_int_field % 5 == 2

    @pytest.mark.usefixtures("icl_test_int_field")
    def test_reverse_integer_arithmetic(self, icl_test_int_field):
        two = IclField[int](description="Two", value=2, type_=int)
        assert 1 + icl_test_int_field == 43
        assert 50 - icl_test_int_field == 8
        assert 2 * icl_test_int_field == 84
        assert 42**two == 1764
        assert 5 / two == 2.5
        assert 5 // two == 2
        assert 5 % two == 1

    @pytest.mark.usefixtures("icl_test_int_field")
    def test_field_arithmetic(self, icl_test_int_field):
        two = IclField[int](description="Two", value=2, type_=int)
        assert icl_test_int_field + two == 44
        assert icl_test_int_field - two == 40
        assert icl_test_int_field * two == 84
        assert icl_test_int_field**two == 1764
        assert icl_test_int_field / (two * two) == 10.5
        assert icl_test_int_field // two == 21
        assert icl_test_int_field % two == 0

    @pytest.mark.usefixtures("icl_test_int_field")
    def test_integer_bit_manipulation(self, icl_test_int_field):
        assert icl_test_int_field << 1 == 84
        assert icl_test_int_field >> 1 == 21
        assert icl_test_int_field & 0b1111 == 0b1010
        assert icl_test_int_field | 0b1111 == 47
        assert icl_test_int_field ^ 0b1111 == 37
        assert ~icl_test_int_field == -43

    def test_reverse_integer_bit_manipulation(self):
        one = IclField[int](description="One", value=1, type_=int)
        fifteen = IclField[int](description="Fifteen", value=15, type_=int)
        assert 42 << one == 84
        assert 42 >> one == 21
        assert 42 & fifteen == 0b1010
        assert 42 | fifteen == 47
        assert 42 ^ fifteen == 37

    @pytest.mark.usefixtures("icl_test_int_field")
    def test_field_bit_manipulation(self, icl_test_int_field):
        one = IclField[int](description="One", value=1, type_=int)
        fifteen = IclField[int](description="Fifteen", value=15, type_=int)
        assert icl_test_int_field << one == 84
        assert icl_test_int_field >> one == 21
        assert icl_test_int_field & fifteen == 0b1010
        assert icl_test_int_field | fifteen == 47
        assert icl_test_int_field ^ fifteen == 37

    @pytest.mark.usefixtures("icl_test_int_field")
    def test_integer_comparison(self, icl_test_int_field):
        assert icl_test_int_field == 42
        assert icl_test_int_field != 0
        assert icl_test_int_field >= 42
        assert icl_test_int_field > 41
        assert icl_test_int_field <= 42
        assert icl_test_int_field < 43

    @pytest.mark.usefixtures("icl_test_fpga_field")
    def test_fpga_integer_comparison(self, icl_test_fpga_field):
        assert icl_test_fpga_field == 42
        assert icl_test_fpga_field != 0
        assert icl_test_fpga_field >= 42
        assert icl_test_fpga_field > 41
        assert icl_test_fpga_field <= 42
        assert icl_test_fpga_field < 43

    @pytest.mark.usefixtures("icl_test_fpga_multibit_field")
    def test_fpga_multibit_integer_comparison(self, icl_test_fpga_multibit_field):
        assert icl_test_fpga_multibit_field == 42
        assert icl_test_fpga_multibit_field != 0
        assert icl_test_fpga_multibit_field >= 42
        assert icl_test_fpga_multibit_field > 41
        assert icl_test_fpga_multibit_field <= 42
        assert icl_test_fpga_multibit_field < 43

    @pytest.mark.usefixtures("icl_test_int_field")
    def test_field_comparison(self, icl_test_int_field):
        forty_one = IclField[int](description="Forty One", value=41, type_=int)
        assert not icl_test_int_field == forty_one
        assert icl_test_int_field != forty_one
        assert icl_test_int_field >= forty_one
        assert icl_test_int_field > forty_one
        assert forty_one <= icl_test_int_field
        assert forty_one < icl_test_int_field

    def test_bool_field(self):
        true_field = IclField[bool](description="true", value=True, type_=bool)
        false_field = IclField[bool](description="false", value=False, type_=bool)
        assert true_field
        assert not false_field
        assert not true_field == false_field

    @pytest.mark.usefixtures("icl_test_fpga_array_field")
    def test_array_read(self, icl_test_fpga_array_field):
        for n in range(4):
            assert icl_test_fpga_array_field[n] == n
            assert icl_test_fpga_array_field.value[n] == n

    @pytest.mark.usefixtures("icl_test_fpga_array_field")
    def test_array_write(self, icl_test_fpga_array_field):
        for n in range(4):
            icl_test_fpga_array_field[n] = 5
            assert icl_test_fpga_array_field[n] == 5
            assert icl_test_fpga_array_field.value[n] == 5

    @pytest.mark.usefixtures("icl_test_fpga_array_field")
    def test_array_index_error(self, icl_test_fpga_array_field):
        with pytest.raises(IndexError):
            icl_test_fpga_array_field[4]
        with pytest.raises(IndexError):
            icl_test_fpga_array_field[4] = 4

    @pytest.mark.parametrize("index", [0, 10, -2])
    def test_read_index(self, simulated_fpga, index):
        array_reg = simulated_fpga.array.data
        assert array_reg[index] == array_reg.value[index]

    @pytest.mark.parametrize(
        "start,end",
        [(0, 1), (2, 10), (-4, -2), (-2, -4), (None, -1), (-2, None)],
    )
    def test_read_slice(self, simulated_fpga, start, end):
        array_reg = simulated_fpga.array.data
        np.testing.assert_equal(array_reg[start:end], array_reg.value[start:end])

    @pytest.mark.parametrize(
        "start,end,step",
        [
            (1022, 1026, None),
            (1025, None, None),
            (-1025, None, None),
            (None, 1025, None),
            (None, -1025, None),
            (10, 20, 2),  # only step 1 supported
        ],
    )
    def test_read_invalid_slice(self, simulated_fpga, start, end, step):
        # array is 1024 elements
        array_reg = simulated_fpga.array.data
        with pytest.raises(Exception):
            array_reg[start:end:step]

    @pytest.mark.parametrize(
        "start,end", [(0, 1), (2, 10), (-4, -2), (None, 2), (-2, None)]
    )
    def test_write_slice(self, simulated_fpga, start, end):
        array_reg = simulated_fpga.array.data
        start_ = start or 0
        end_ = end or 0
        length = abs(end_ - start_)
        data = np.random.randint(0, 1 << 31, length, dtype=ArgsWordType)
        array_reg[start:end] = data
        np.testing.assert_equal(array_reg.value[start:end], data)

    @pytest.mark.parametrize(
        "start,end,step",
        [
            (1022, 1026, None),
            (1025, None, None),
            (-1025, None, None),
            (None, 1025, None),
            (None, -1025, None),
            (10, 20, 2),  # only step 1 supported
        ],
    )
    def test_write_invalid_slice(self, simulated_fpga, start, end, step):
        # array is 1024 elements
        array_reg = simulated_fpga.array.data
        length = abs((end or array_reg.length) - (start or 0)) // (step or 1)
        data = np.random.randint(0, 1 << 31, length, dtype=ArgsWordType)
        with pytest.raises(Exception):
            array_reg[start:end:step] = data


class TestFpgaPeripheral:
    @pytest.mark.usefixtures("mock_driver")
    def test_init(self, mock_driver):
        FpgaPeripheral(mock_driver, my_fields)

    @pytest.mark.usefixtures("mock_driver")
    def test_read_subscript(self, mock_driver):
        peripheral = FpgaPeripheral(mock_driver, my_fields)
        field_address = my_fields["my_field"].address
        read_count = mock_driver.read_log.count(field_address)
        # read value
        peripheral["my_field"].value
        # number of reads of the address should increment
        assert mock_driver.read_log.count(field_address) > read_count

    @pytest.mark.usefixtures("mock_driver")
    def test_read_attr(self, mock_driver):
        peripheral = FpgaPeripheral(mock_driver, my_fields)
        field_address = my_fields["my_field"].address
        read_count = mock_driver.read_log.count(field_address)
        # read value
        peripheral.my_field.value
        # number of reads of the address should increment
        assert mock_driver.read_log.count(field_address) > read_count

    @pytest.mark.usefixtures("mock_driver")
    def test_write_subscript(self, mock_driver):
        peripheral = FpgaPeripheral(mock_driver, my_fields)
        field_address = my_fields["my_field"].address
        write_count = mock_driver.write_address_log.count(field_address)
        # write value
        assert 456 not in mock_driver.write_value_log
        peripheral["my_field"].value = 456
        # number of writes of the address should increment
        assert mock_driver.write_address_log.count(field_address) > write_count
        assert 456 in mock_driver.write_value_log

    @pytest.mark.usefixtures("mock_driver")
    @pytest.mark.parametrize(
        "write_value",
        [
            456,
            np.uint32(123),
            np.array([1, 2, 3, 4], dtype=np.uint8),
            True,
            IclField[int](value=987, type_=int),
            IclField[int](
                value=np.array([1, 2], dtype=ArgsWordType), type_=int, length=2
            ),
            IclField[bool](value=False, type_=bool),
        ],
    )
    def test_write_attr(self, mock_driver, write_value):
        peripheral = FpgaPeripheral(mock_driver, my_fields)
        field_address = my_fields["my_field"].address
        write_count = mock_driver.write_address_log.count(field_address)
        # write value
        assert write_value not in mock_driver.write_value_log
        peripheral.my_field.value = write_value
        # number of writes of the address should increment
        assert mock_driver.write_address_log.count(field_address) > write_count
        # we can't do a simple "assert x in y" here because it doesn't work
        # with numpy arrays
        assert np.all(write_value == mock_driver.write_value_log[-1])

    def test_attribute_discovery(self, mock_driver):
        """Check that properties are discovered and read/write detected"""

        class AttrTestClass(FpgaPeripheral):
            @property
            def test_ro(self) -> IclFpgaField:
                return IclFpgaField(user_write=False)

            @property
            def test_rw(self) -> IclFpgaField:
                return IclFpgaField(user_write=True)

            @test_rw.setter
            def test_rw(self, val):
                pass

        attr_test_class = AttrTestClass(mock_driver, my_fields)
        assert len(attr_test_class.user_attributes) == 2
        assert all(x in attr_test_class.user_attributes for x in ["test_ro", "test_rw"])
        assert attr_test_class.test_ro.user_write is False
        assert attr_test_class.test_rw.user_write is True

    def test_attribute_expose_all(self, mock_driver):
        """Check that attribute discovery can be affected by
        DEBUG_FPGA environment variable."""

        class AttrTestClass(FpgaPeripheral):
            _user_attributes = {
                "test_attrib",
            }

            _user_methods = {"method_a"}

            def method_a(self):
                return

            def method_b(self):
                return

        for debug in (False, True):
            os.environ["DEBUG_FPGA"] = str(debug)
            attr_test_class = AttrTestClass(mock_driver, my_fields)
            assert attr_test_class._expose_all == debug
            if debug:
                assert "my_field" in attr_test_class.user_attributes
                assert "method_b" in attr_test_class.user_methods
            else:
                assert "my_field" not in attr_test_class.user_attributes
                assert "method_b" not in attr_test_class.user_methods

    def test_expose_all_override_none(self, mock_driver):
        """Check that attribute discovery can be affected by
        DEBUG_FPGA environment variable and overrides None"""

        class AttrTestClass(FpgaPeripheral):
            _user_attributes = None

            _user_methods = None

            def method_a(self):
                return

        for debug in (False, True, False):
            os.environ["DEBUG_FPGA"] = str(debug)
            attr_test_class = AttrTestClass(mock_driver, my_fields)
            assert attr_test_class._expose_all == debug
            if debug:
                assert "my_field" in attr_test_class.user_attributes
                assert "method_a" in attr_test_class.user_methods
            else:
                assert "my_field" not in attr_test_class.user_attributes
                assert "method_a" not in attr_test_class.user_methods

    def test_expose_all_override_not_user(self, mock_driver):
        """Check that attribute discovery can be affected by
        DEBUG_FPGA environment variable and overrides the _not_user settings"""

        class AttrTestClass(FpgaPeripheral):
            _not_user_attributes = {"my_field"}
            _not_user_methods = {"method_a"}

            def method_a(self):
                return

        for debug in (False, True, False):
            os.environ["DEBUG_FPGA"] = str(debug)
            attr_test_class = AttrTestClass(mock_driver, my_fields)
            assert attr_test_class._expose_all == debug
            if debug:
                assert "my_field" in attr_test_class.user_attributes
                assert "method_a" in attr_test_class.user_methods
            else:
                assert "my_field" not in attr_test_class.user_attributes
                assert "method_a" not in attr_test_class.user_methods

    def test_attr_discover_properties(self, mock_driver):
        """Test discover properties flag"""

        class AttrTestClass(FpgaPeripheral):
            _user_attributes = {"test_ro", DISCOVER_PROPERTIES}

            @property
            def test_ro(self) -> IclFpgaField:
                return IclFpgaField(user_write=False)

            @property
            def test_rw(self) -> IclFpgaField:
                return IclFpgaField(user_write=True)

        attr_test_class = AttrTestClass(mock_driver, my_fields)
        assert "test_rw" in attr_test_class.user_attributes

    def test_attr_discover_registers(self, mock_driver):
        """Test register discovery flag"""

        class AttrTestClass(FpgaPeripheral):
            _user_attributes = {"test_ro", DISCOVER_REGISTERS}

            @property
            def test_ro(self) -> IclFpgaField:
                return IclFpgaField(user_write=False)

        attr_test_class = AttrTestClass(mock_driver, my_fields)
        assert "my_field" in attr_test_class.user_attributes

    def test_attr_discover_all(self, mock_driver):
        """Test discover all flag"""

        class AttrTestClass(FpgaPeripheral):
            _user_attributes = {"test_ro", DISCOVER_ALL}

            @property
            def test_ro(self) -> IclFpgaField:
                return IclFpgaField(user_write=False)

            @property
            def test_rw(self) -> IclFpgaField:
                return IclFpgaField(user_write=True)

        attr_test_class = AttrTestClass(mock_driver, my_fields)
        assert "test_rw" in attr_test_class.user_attributes
        assert "my_field" in attr_test_class.user_attributes

    def test_user_attributes(self, mock_driver):
        """Check that setting _user_attributes replaces discovery behaviour"""

        class AttrTestClass(FpgaPeripheral):
            _user_attributes = {"test_ro"}

            @property
            def test_ro(self) -> IclFpgaField:
                return IclFpgaField(user_write=False)

            @property
            def test_rw(self) -> IclFpgaField:
                return IclFpgaField(user_write=True)

            @test_rw.setter
            def test_rw(self, val):
                pass

        attr_test_class = AttrTestClass(mock_driver, my_fields)
        assert len(attr_test_class.user_attributes) == 1
        assert "test_ro" in attr_test_class.user_attributes

    def test_no_user_attributes(self, mock_driver):
        """Check that _user_attributes=None disables discovery behaviour"""

        class AttrTestClass(FpgaPeripheral):
            _user_attributes = None

            @property
            def test_ro(self) -> IclFpgaField:
                return IclFpgaField(user_write=False)

            @property
            def test_rw(self) -> IclFpgaField:
                return IclFpgaField(user_write=True)

            @test_rw.setter
            def test_rw(self, val):
                pass

        attr_test_class = AttrTestClass(mock_driver, my_fields)
        assert len(attr_test_class.user_attributes) == 0

    def test_not_user_attributes(self, mock_driver):
        """Check that _not_user_attributes excludes things from discovery"""

        class AttrTestClass(FpgaPeripheral):
            _not_user_attributes = {"test_ro"}

            @property
            def test_ro(self) -> IclFpgaField:
                return IclFpgaField(user_write=False)

            @property
            def test_rw(self) -> IclFpgaField:
                return IclFpgaField(user_write=True)

            @test_rw.setter
            def test_rw(self, val):
                pass

        attr_test_class = AttrTestClass(mock_driver, my_fields)
        assert "test_ro" not in attr_test_class.user_attributes

    def test_method_discovery(self, mock_driver):
        """Test default method discovery"""

        class MethodTestClass(FpgaPeripheral):
            def method_a(self):
                return

            def method_b(self):
                return

        method_test_class = MethodTestClass(mock_driver, my_fields)
        assert "method_a" in method_test_class.user_methods
        assert "method_b" in method_test_class.user_methods

    def test_user_methods(self, mock_driver):
        """Test override of method discovery"""

        class MethodTestClass(FpgaPeripheral):
            _user_methods = {"method_a"}

            def method_a(self):
                return

            def method_b(self):
                return

        method_test_class = MethodTestClass(mock_driver, my_fields)
        assert "method_a" in method_test_class.user_methods
        assert "method_b" not in method_test_class.user_methods

    def test_user_methods_none(self, mock_driver):
        """Test disabling of method discovery"""

        class MethodTestClass(FpgaPeripheral):
            _user_methods = None

            def method_a(self):
                return

            def method_b(self):
                return

        method_test_class = MethodTestClass(mock_driver, my_fields)
        assert "method_a" not in method_test_class.user_methods
        assert "method_b" not in method_test_class.user_methods

    def test_not_user_methods(self, mock_driver):
        """Test exclusion from method discovery"""

        class MethodTestClass(FpgaPeripheral):
            _not_user_methods = {"method_a"}

            def method_a(self):
                return

            def method_b(self):
                return

        method_test_class = MethodTestClass(mock_driver, my_fields)
        assert "method_a" not in method_test_class.user_methods
        assert "method_b" in method_test_class.user_methods

    def test_field_config(self, mock_driver):
        """Check that setting values in _field_config overwrites the defaults,
        and that values that are not set retain the default values."""

        class FieldTestClass(FpgaPeripheral):
            _field_config = {
                "my_field": IclFpgaField(description="new desc.", type_=str)
            }

        field_test_class = FieldTestClass(mock_driver, my_fields)
        assert field_test_class.my_field.type_ == str
        assert field_test_class.my_field.address == my_fields["my_field"].address
        assert (
            field_test_class.my_field.description != my_fields["my_field"].description
        )
        assert field_test_class.my_field.description == "new desc."

    def test_no_attr_leakage(self, mock_driver):
        """Make sure user attributes are contained to their peripheral"""

        class AttrTest1(FpgaPeripheral):
            @property
            def test_1(self) -> IclFpgaField:
                return IclFpgaField()

        class AttrTest2(FpgaPeripheral):
            @property
            def test_2(self) -> IclFpgaField:
                return IclFpgaField()

        peripheral1 = AttrTest1(mock_driver, my_fields)
        peripheral2 = AttrTest2(mock_driver, my_fields)
        assert "test_2" not in peripheral1.user_attributes
        assert "test_1" not in peripheral2.user_attributes

    def test_no_config_leakage(self, mock_driver):
        """Make sure user attribute configuration settings don't leak out"""

        class AttrTestClass(FpgaPeripheral):
            _user_attributes = {"test_ro", DISCOVER_REGISTERS}

            @property
            def test_ro(self) -> IclFpgaField:
                return IclFpgaField(user_write=False)

        attr_test_class = AttrTestClass(mock_driver, my_fields)
        assert DISCOVER_REGISTERS not in attr_test_class.user_attributes


class TestFpgaPersonality:
    def test_class_map(self, mock_driver):
        """Check that peripheral_class is used to instantiate peripheral objects,
        defaulting to FpgaPeripheral if no class is specified"""
        _peripheral_class = {"test_peri_1": Mock}
        with patch(
            "ska_low_cbf_fpga.fpga_icl.FpgaPersonality._peripheral_class",
            _peripheral_class,
        ):
            personality = FpgaPersonality(mock_driver, DummyArgsMap())
            assert isinstance(personality.test_peri_1, Mock)
            assert isinstance(personality.test_peri_2, FpgaPeripheral)

    @pytest.mark.usefixtures("personality")
    def test_dir(self, personality):
        """Check that peripherals are listed in __dir__"""
        assert all(
            peripheral in dir(personality) for peripheral in DummyArgsMap().keys()
        )

    def test_user_attr(self, mock_driver):
        test_attributes = {"foo", "bar"}

        class TestPersonality(FpgaPersonality):
            _user_attributes = test_attributes

            @property
            def foo(self) -> IclField[str]:
                return IclField(type_=str, value="FOO!")

            @property
            def bar(self) -> IclField[str]:
                return IclField(type_=str, value="BAR!")

        personality = TestPersonality(mock_driver, DummyArgsMap())
        assert all(x in personality.user_attributes for x in test_attributes)

    @pytest.mark.usefixtures("simulated_fpga")
    def test_fw_personality(self, simulated_fpga):
        """Verify the fw_personality property."""
        assert simulated_fpga.fw_personality.value == "test"

    @pytest.mark.usefixtures("simulated_fpga")
    def test_fw_version(self, simulated_fpga):
        """Verify the fw_version property."""
        # release build type
        simulated_fpga.system.build_type = int.from_bytes(b" rel", byteorder="big")
        assert simulated_fpga.fw_version.value == "1.2.3"

        # development build type
        simulated_fpga.system.build_type = int.from_bytes(b"dev ", byteorder="big")
        assert simulated_fpga.fw_version.value == "1.2.3-dev.01020304"

    @pytest.mark.parametrize("build_type", [b" REL", b"DEV "])
    @pytest.mark.usefixtures("simulated_fpga")
    def test_check_fw(self, simulated_fpga, build_type):
        """Exercise the _check_fw method."""
        simulated_fpga.system.build_type = int.from_bytes(build_type, byteorder="big")
        # spec that matches FPGA simulation, should be no exception raised
        simulated_fpga._check_fw("test", ">=1.2.3")

        with pytest.raises(Exception):
            # wrong personality name
            simulated_fpga._check_fw("blah", "*")
        with pytest.raises(Exception):
            # wrong version (FPGA is 1.2.3)
            simulated_fpga._check_fw("test", ">=1.2.4")


@pytest.mark.usefixtures("simulated_fpga")
class TestIclFpgaBitfield:
    def test_value_type(self, simulated_fpga):
        assert isinstance(simulated_fpga.system.eth100g_fec_enable.value, bool)

    def test_read(self, simulated_fpga):
        simulated_fpga.system.eth100g_fec_whole_bitfield = 0
        assert not simulated_fpga.system.eth100g_fec_enable.value
        simulated_fpga.system.eth100g_fec_whole_bitfield = 0x10
        assert simulated_fpga.system.eth100g_fec_enable.value

    def test_write(self, simulated_fpga):
        # check that setting to True does not set other bits
        simulated_fpga.system.eth100g_fec_whole_bitfield = 0
        simulated_fpga.system.eth100g_fec_enable = True
        assert simulated_fpga.system.eth100g_fec_enable.value
        # this bit is at offset 4 per FPGA map
        assert simulated_fpga.system.eth100g_fec_whole_bitfield.value == 0x10

        # confirm that setting to False does not reset other bits
        simulated_fpga.system.eth100g_fec_whole_bitfield = 0xFFFF_FFFF
        simulated_fpga.system.eth100g_fec_enable = False
        assert not simulated_fpga.system.eth100g_fec_enable.value
        assert simulated_fpga.system.eth100g_fec_whole_bitfield.value == 0xFFFF_FFEF
